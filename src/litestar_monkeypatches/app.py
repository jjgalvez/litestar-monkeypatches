from typing import Any
from litestar import Litestar

Litestar._route_reverse = Litestar.route_reverse

def route_reverse(self, name: str,
                query_parameters: dict[str, str] = {},
                path_parameters: dict[str, Any] = {},
                **other_path_parameters: Any) -> str:

    # allow any keyword path parameters to override explicitly set path_parameters
    path_parameters.update(other_path_parameters)

    path = self._route_reverse(name=name, **path_parameters)

    if query_parameters:
        q = []
        for k,v in query_parameters.items():
            q.append(f'{k}={v}')
        return f'{path}?{"&".join(q)}'
    return path

def jls_extract_def():
    
    return 


Litestar.route_reverse = route_reverse

__all__ = [
    'Litestar'
]



